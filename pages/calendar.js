import Layout from '../components/MyLayout';
// import React from 'react'
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment';
import React, { Component } from 'react';
// import events from "./event";
// import 'react-big-calendar/lib/css/react-big-calendar.css'
import '../styles/calendar.scss'
// import fetch from 'isomorphic-unfetch';
// import Link from 'next/link';
import axios from 'axios'
// import BigCalendar from 'react-big-calendar';


// const localizer = momentLocalizer(moment)
// const MyCalendar = props => (
//   <div>
//     <Layout>
//     <Calendar
//       localizer={localizer}
//       events={cal_events}
//       startAccessor="start"
//       endAccessor="end"
//       style={{height: 500}}
//       defaultDate={new Date()}
//     />
//     </Layout>
//     <div>
//     <div>
//        <h1>Batman TV Shows</h1>
//     <ul>
//       {props.data.map(each => (
//         <li> {each.title}
//         </li>
//       ))}
//     </ul>
//   </div>
//     </div>
//   </div>
// )

// MyCalendar.getInitialProps = async function() {
//   const res = await fetch('http://localhost:3000/appointments');
//   const data = await res.json();

//   console.log(`Show data fetched. Count: ${data.length}`);
//   for (let i = 0; i < data.length; i++) {
//     data[i].start = moment.utc(data[i].start).toDate();
//     data[i].end = moment.utc(data[i].end).toDate();
//     console.log(data)
    
//   }
//   self.setState({
//     cal_events:data
//   })
//   return {
//     data
//   };
// };
moment.locale('en-GB');


// Calendar.momentLocalizer(moment);
const localizer = momentLocalizer(moment)
class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      cal_events: [
        //State is updated via componentDidMount
      ],
    }

  }
  handleSelect = ({ title }) => {

  }
  convertDate = (date) => {
    return moment.utc(date).toDate()
  }

  componentDidMount() {


    axios.get('http://localhost:3000/appointments')
      .then(response => {
        console.log(response.data);
        let appointments = response.data;
        
        for (let i = 0; i < appointments.length; i++) {
          
          appointments[i].start = this.convertDate(appointments[i].start)
          appointments[i].end = this.convertDate(appointments[i].end)
          console.log(appointments)
        }

        this.setState({
          cal_events:appointments
        })
  
      })
      .catch(function (error) {
        console.log(error);
      });
  }


  render() {

    const { cal_events } = this.state

    return (
      <div>
          <Layout>
            <div style={{ height: 500 , width: '50%', margin: 'auto' }}>
              <Calendar
                events={cal_events}
                localizer={localizer}
                step={30}
                defaultView='month'
                views={['month','week','day']}
                defaultDate={new Date()}
                onSelectEvent={event => alert('Create by : '+event.createby)}
                onSelectSlot={this.handleSelect}
              />
            </div>
            </Layout>
      </div>

    );
  }
}

export default App;
// export default MyCalendar
