import Layout from '../components/MyLayout';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import axios from 'axios'
import React, { Component } from 'react'

export default class Booking extends React.Component {
  state = {
    start: '',
    end:'',
    title:'',
    priority:'',
    createby:''
  }

  handleChangestart = event => {
    this.setState({ start: event.target.value });
  }
  handleChangeend = event => {
    this.setState({ end: event.target.value });
  }
  handleChangetitle = event => {
    this.setState({ title: event.target.value });
  }
  handleChangecreateby = event => {
    this.setState({ createby: event.target.value });
  }
  handleChangepriority = event => {
    this.setState({ priority: event.target.value });
  }


  handleSubmit = event => {
    console.log("prepare")
    event.preventDefault();

    const user = {
      "start": this.state.start,
      "end": this.state.end,
      "title": this.state.title,
      "priority":this.state.priority,
      "createby":this.state.createby
    };
    console.log(user)
    axios.post(`http://localhost:3000/appointments`,  user )
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render() {
    return (
      <div>
        <Layout>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label>
              Start :
              <input type="text" name="name" onChange={this.handleChangestart} />
            </label>
          </div>
          <div>
            <label>
              End date:
              <input type="text" name="name" onChange={this.handleChangeend} />
            </label>
          </div>
          <div>
            <label>
              title:
              <input type="text" name="name" onChange={this.handleChangetitle} />
            </label>
          </div>
          <div>
            <label>
              create by:
              <input type="text" name="name" onChange={this.handleChangecreateby} />
            </label>
          </div>
          <div>
            <label>
              priority:
              <input type="number" name="name" onChange={this.handleChangepriority} />
            </label>
          </div>
          <div>
            <button type="submit">Add</button>
          </div>

        </form>
        </Layout>
      </div>
    )
  }
}