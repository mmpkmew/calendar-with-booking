import Link from 'next/link';

const linkStyle = {
  marginRight: 15
};

const Header = () => (
  <div>
    <Link href="/">
      <a style={linkStyle}>Booking</a>
    </Link>
    <Link href="/calendar">
      <a style={linkStyle}>Calendar</a>
    </Link>
  </div>
);

export default Header;